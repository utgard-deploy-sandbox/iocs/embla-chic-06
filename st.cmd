#!/usr/bin/env iocsh.bash
require(modbus)
require(s7plc)
require(calc)
epicsEnvSet(labs-embla_chop-chic-06_VERSION,"plcfactory")
iocshLoad("$(E3_COMMON_DIR)/e3-common.iocsh")
iocshLoad("./labs-embla_chop-chic-06.iocsh","IPADDR=172.30.235.21,RECVTIMEOUT=3000")

#Load alarms database
epicsEnvSet(P,"LabS-Embla:")
epicsEnvSet(R,"Chop-Drv-0601:")
dbLoadRecords("./SKFAlrm.db", "P=$(P), R=$(R)")
iocInit()
#EOF
